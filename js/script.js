const errorCSS = 'val-error';
const nombre = document.getElementById("nombre");

const setErrorMsg = setP => {

    let control = document.querySelector("#nombre + .cand-error");

    if (setP) {

        control.classList.add(errorCSS);
        control.innerText = "Por favor, introduce un nombre";
        return true;
    }

    control.classList.remove(errorCSS);
}


nombre.addEventListener("invalid", event => {
    event.preventDefault();
    setErrorMsg(true);
})

nombre.addEventListener("input", () => {
    setErrorMsg(false);
})